import numpy as np 
from qiskit import *

import qiskit.tools.visualization as v
import matplotlib

#%matplotlib inline
matplotlib.use("TkAgg")

from qiskit import BasicAer

#BORRARfrom qiskit.visualization import plot_state_city

#load credentials
with open('ibm_q.token', 'r') as t:
    token = t.readline()
IBMQ.save_account(token, overwrite=True)
IBMQ.load_accounts()

######################################################

# Create Quantum Reg with 2 qubits
q = QuantumRegister(2,'q')

# Create Classical Reg with 2 bits
c = ClassicalRegister(2, 'c')

# Create Quantum Circuit acting on q registers
qc = QuantumCircuit(q, c)

#h q[0];
qc.h(q[0])

#h q[1];
qc.h(q[1])

#h q[1];
qc.h(q[1])

#cx q[0],q[1];
qc.cx(q[0], q[1])

#h q[0];
qc.h(q[0])

#h q[1];
qc.h(q[1])

#x q[0];
qc.x(q[0])

#h q[1];
qc.h(q[1])

#x q[1];
qc.x(q[1])

#h q[1];
qc.h(q[1])

#cx q[0],q[1];
qc.cx(q[0], q[1])

#x q[0];
qc.x(q[0])

#h q[1];
qc.h(q[1])

#h q[0];
qc.h(q[0])

#x q[1];
qc.x(q[1])

#h q[1];
qc.h(q[1])

# add barrier
qc.barrier(q)

#Draw circuit
qc.draw()



#measure q[0] -> c[0];
#measure q[1] -> c[1];
qc.measure(q, c)

# simulate circuits
job = execute(qc, BasicAer.get_backend('qasm_simulator'), shots = 100)
res = job.result()
counts = res.get_counts(qc)

#outputs
print(counts)

v.plot_histogram(counts)

v.circuit_drawer(qc, scale=1.0, filename='circuit.png', output = 'mpl')




