#!/usr/bin/env python
# -*- coding: latin-1 -*-

# Ricardo Román-Brenes
# ricardo.roman@ucr.ac.cr
# 29 may 2019
# Quantum Computing workshop @ IBM
# Aqua package example

# import Quantum package
import qiskit as qk
import qiskit.tools.visualization as v
import matplotlib
matplotlib.use("TkAgg")



# load credentials
with open('ibm_q.token', 'r') as t:
    token = t.readline()
qk.IBMQ.save_account(token, overwrite=True)
qk.IBMQ.load_accounts()

# check available execution backends
# print(qk.IBMQ.backends())

# create 4 qbits named q.
q_regs = qk.QuantumRegister(4, 'q')

# create circuit using qbits.
qc = qk.QuantumCircuit(q_regs)

# create 4 qbits for measurement
c_regs = qk.ClassicalRegister(4, 'm')

# create circuit with both registers
m = qk.QuantumCircuit(q_regs, c_regs)

# add Hadamard gates
m.h( q_regs[0] )
m.h( q_regs[1] )

# add control-not  gates
m.cx(q_regs[0], q_regs[2])
m.cx(q_regs[1], q_regs[3])

# add flip gates
m.x(q_regs[2])
m.x(q_regs[3])

# add barrier
m.barrier(q_regs)

# collapse quantum states
m.measure(q_regs,c_regs)

# simulate circuits
job = qk.execute(m, qk.BasicAer.get_backend('qasm_simulator'), shots=100)
res = job.result()
counts = res.get_counts(m)

# outputs (don't work too well in jupyter)
print(counts)

v.plot_histogram(counts) # .savefig('qk_histogram.png')
v.circuit_drawer(m, scale=1.0, filename='qk_circuit.png', output='mpl')


